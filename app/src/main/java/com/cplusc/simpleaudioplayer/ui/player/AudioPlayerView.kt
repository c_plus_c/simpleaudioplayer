package com.cplusc.simpleaudioplayer.ui.player

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.cplusc.simpleaudioplayer.R
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class AudioPlayerView : ConstraintLayout {

    private var durationInMillisecond: Long = 0
    
    private var currentTimeText: TextView
    private var seekbar: SeekBar
    private var durationText: TextView

    private var skipPreviousImage: ImageView
    private var playImage: ImageView
    private var skipNextImage: ImageView

    private var isPlaying: Boolean = true

    private val playButtonClickSubject: Subject<Unit> = PublishSubject.create()
    val playButtonClickObservable: Observable<Unit>
        get() = playButtonClickSubject


    private val pauseButtonClickSubject: Subject<Unit> = PublishSubject.create()
    val pauseButtonClickObservable: Observable<Unit>
        get() = pauseButtonClickSubject

    private val skipPreviousClickSubject: Subject<Unit> = PublishSubject.create()
    val skipPreviousClickObservable: Observable<Unit>
        get() = skipPreviousClickSubject

    private val skipNextClickSubject: Subject<Unit> = PublishSubject.create()
    val skipNextClickObservable: Observable<Unit>
        get() = skipNextClickSubject

    private val seekedSubject: Subject<Long> = PublishSubject.create()
    val seekedObservable: Observable<Long>
        get() = seekedSubject

    constructor(context: Context) : super(context, null)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    
    private fun play(){
        isPlaying = true
        playImage.setImageResource(R.drawable.pause)
        playButtonClickSubject.onNext(Unit)
    }
    
    private fun pause(){
        isPlaying = false
        playImage.setImageResource(R.drawable.play)
        pauseButtonClickSubject.onNext(Unit)
    }
    
    // constructorの中で書くとアウトらしい
    init{
        val view = LayoutInflater.from(context).inflate(R.layout.player_view, this)

        currentTimeText = view.findViewById(R.id.player_view_current_time_text)!!
        seekbar = view.findViewById(R.id.player_view_seek_bar)!!
        durationText = view.findViewById(R.id.player_view_duration_text)!!

        skipPreviousImage = view.findViewById(R.id.player_view_skip_previous)!!
        playImage = view.findViewById(R.id.player_view_play)!!
        skipNextImage = view.findViewById(R.id.player_view_skip_next)!!

        playImage.setOnClickListener {
            if (isPlaying) {
                pause()
            } else {
                play()
            }
        }
        
        skipPreviousImage.setOnClickListener { 
            skipPreviousClickSubject.onNext(Unit)
        }
        
        skipNextImage.setOnClickListener { 
            skipNextClickSubject.onNext(Unit)
        }
        
        seekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                // do nothing
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                // do nothing
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                val currentTime = seekbar.progress.toLong() * durationInMillisecond / 500
                seekedSubject.onNext(currentTime)
            }
        })
    }

    fun initTrack(durationInMillisecond: Long) {
        this.durationInMillisecond = durationInMillisecond
        durationText.text = getTimeFormatString(durationInMillisecond)
        play()
    }

    fun setCurrentTime(currentTimeInMillisecond: Long) {
        seekbar.progress = (currentTimeInMillisecond * 500 / durationInMillisecond).toInt()
        currentTimeText.text = getTimeFormatString(currentTimeInMillisecond)
    }
    
    private fun getTimeFormatString(timeInMillisecond: Long): String{
        val seconds = (timeInMillisecond / 1000) % 60
        val minutes = (timeInMillisecond / (1000 * 60) % 60)
        return "%02d:%02d".format(minutes, seconds)
    }
}