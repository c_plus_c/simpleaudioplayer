package com.cplusc.simpleaudioplayer.ui.screen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cplusc.simpleaudioplayer.R
import com.cplusc.simpleaudioplayer.domain.AudioTrack
import io.gresse.hugo.vumeterlibrary.VuMeterView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class TrackListAdapter() : RecyclerView.Adapter<TrackListAdapter.TrackListItemViewHolder>() {

    private val clickedSubject: Subject<AudioTrack> = PublishSubject.create()
    val clickedObservable: Observable<AudioTrack>
        get() = clickedSubject

    var playingTrackIndex: Int = -1
    var audioTrackList: List<AudioTrack> = mutableListOf()
    private var playOrPause: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackListItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.track_list_item, parent, false)
        return TrackListItemViewHolder(view)
    }

    override fun getItemCount(): Int =
            audioTrackList.count()

    override fun onBindViewHolder(holder: TrackListItemViewHolder, position: Int) {
        holder.setData(audioTrackList[position], position == playingTrackIndex)
    }

    fun setData(audioTrackList: List<AudioTrack>) {
        this.audioTrackList = audioTrackList
        notifyDataSetChanged()
    }

    fun setPlayingTrack(audioTrack: AudioTrack) {
        val previousPlayingTrackIndex = playingTrackIndex

        playingTrackIndex = audioTrackList.indexOf(audioTrack)
        if (previousPlayingTrackIndex != -1) {
            notifyItemChanged(previousPlayingTrackIndex)
        }
        notifyItemChanged(playingTrackIndex)
    }
    
    fun setPlayOrPause(playOrPause: Boolean){
        this.playOrPause = playOrPause
        notifyItemChanged(playingTrackIndex)
    }

    inner class TrackListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val meterView: VuMeterView = itemView.findViewById(R.id.track_list_item_meter)
        val trackNameTextView: TextView = itemView.findViewById(R.id.track_list_item_track_name)

        fun setData(audioTrack: AudioTrack, isPlaying: Boolean) {
            trackNameTextView.text = audioTrack.title

            itemView.setOnClickListener { _ ->
                clickedSubject.onNext(audioTrack)
            }

            if (isPlaying) {
                meterView.visibility = View.VISIBLE
            } else {
                meterView.visibility = View.INVISIBLE
            }
            
            if(playOrPause){
                meterView.resume(true)
            }else{
                meterView.stop(true)
            }
        }
    }
}