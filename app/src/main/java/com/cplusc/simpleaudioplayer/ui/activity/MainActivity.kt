package com.cplusc.simpleaudioplayer.ui.activity

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.cplusc.simpleaudioplayer.R
import com.cplusc.simpleaudioplayer.domain.AudioTrack
import com.cplusc.simpleaudioplayer.service.AudioPlayService
import com.cplusc.simpleaudioplayer.ui.player.AudioPlayerView
import com.cplusc.simpleaudioplayer.ui.screen.TrackListFragment
import com.cplusc.simpleaudioplayer.ui.screen.TrackListViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class MainActivity : AppCompatActivity() {

    private lateinit var trackListViewModel: TrackListViewModel

    private val compositeDisposable = CompositeDisposable()

    private lateinit var playerView: AudioPlayerView

    private var audioPlayService: AudioPlayService? = null
    
    private val playingAudioChangedSubject: Subject<AudioTrack> = PublishSubject.create()
    val playingAudioChangedObservable: Observable<AudioTrack>
        get() = playingAudioChangedSubject


    private val playOrPauseChangedSubject: Subject<Boolean> = PublishSubject.create()
    val playOrPauseChangedObservable: Observable<Boolean>
        get() = playOrPauseChangedSubject

    private val audioPlayServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            audioPlayService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            audioPlayService = (service as AudioPlayService.LocalBinder).getService()
            audioPlayService?.let { audioPlayService ->
                audioPlayService.loadFinishedObservable
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            playingAudioChangedSubject.onNext(audioPlayService.currentPlayingAudioTrack)
                            playerView.initTrack(audioPlayService.getDurationInMilliSecond())
                        }.addTo(compositeDisposable)

                audioPlayService.currentPositionChangedObservable
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { currentTimeInMillisecond ->
                            playerView.setCurrentTime(currentTimeInMillisecond)
                        }?.addTo(compositeDisposable)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(Intent(this@MainActivity, AudioPlayService::class.java))
        } else {
            startService(Intent(this@MainActivity, AudioPlayService::class.java))
        }
        bindService(Intent(this@MainActivity, AudioPlayService::class.java), audioPlayServiceConnection, Context.BIND_AUTO_CREATE)

        trackListViewModel = ViewModelProviders.of(this).get(TrackListViewModel::class.java)

        playerView = findViewById(R.id.player_view)

        playerView.playButtonClickObservable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    audioPlayService?.play()
                    playOrPauseChangedSubject.onNext(true)
                }.addTo(compositeDisposable)


        playerView.pauseButtonClickObservable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    audioPlayService?.pause()
                    playOrPauseChangedSubject.onNext(false)
                }.addTo(compositeDisposable)

        playerView.seekedObservable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { seekedTime ->
                    audioPlayService?.seekTo(seekedTime)
                }.addTo(compositeDisposable)


        playerView.pauseButtonClickObservable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    audioPlayService?.pause()
                }.addTo(compositeDisposable)


        val trackListFragment = supportFragmentManager.findFragmentById(R.id.track_list_fragment) as TrackListFragment
        trackListFragment.clickedAudioTrackObservable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { audioTrack ->
                    playerView.visibility = View.VISIBLE
                    audioPlayService?.load(audioTrack)
                }.addTo(compositeDisposable)


        playerView.skipPreviousClickObservable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    audioPlayService?.currentPlayingAudioTrack?.let {
                        val previousTrack = trackListViewModel.resolvePreviousTrack(it)
                        audioPlayService?.load(previousTrack)
                    }
                }.addTo(compositeDisposable)

        playerView.skipNextClickObservable
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { _ ->
                    audioPlayService?.currentPlayingAudioTrack?.let {
                        val nextTrack = trackListViewModel.resolveNextTrack(it)
                        audioPlayService?.load(nextTrack)
                    }
                }.addTo(compositeDisposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
        stopService(Intent(this, AudioPlayService::class.java))
    }
}
