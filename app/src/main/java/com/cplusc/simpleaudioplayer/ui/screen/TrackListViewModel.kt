package com.cplusc.simpleaudioplayer.ui.screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cplusc.simpleaudioplayer.domain.AudioTrack
import com.cplusc.simpleaudioplayer.domain.AudioTracksProvider
import com.cplusc.simpleaudioplayer.infrastructure.AudioTracksProviderFuelImpl
import com.github.kittinunf.fuel.core.FuelError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TrackListViewModel : ViewModel() {
    private val defaultAudioTracksProvider: AudioTracksProvider = AudioTracksProviderFuelImpl()

    private val internalAudioTrackListObservable: MutableLiveData<List<AudioTrack>> = MutableLiveData()

    val audioTrackListObservable: LiveData<List<AudioTrack>> = internalAudioTrackListObservable

    private val internalErrorObservable: MutableLiveData<Exception> = MutableLiveData()
    val errorObservable: LiveData<Exception> = internalErrorObservable

    val isLoading: Boolean
        get() = internalIsLoading

    var internalIsLoading = false

    fun fetchAudioTracks() {
        internalIsLoading = true
        GlobalScope.launch {
            try {
                val fetchedData = defaultAudioTracksProvider.fetchAudioTracks()
                GlobalScope.launch(Dispatchers.Main) {
                    internalAudioTrackListObservable.value = fetchedData
                    internalIsLoading = false
                }
            } catch (e: FuelError) {
                GlobalScope.launch(Dispatchers.Main) {
                    internalErrorObservable.value = e
                    internalIsLoading = false
                }
            }
        }
    }


    fun resolvePreviousTrack(targetAudioTrack: AudioTrack): AudioTrack =
            defaultAudioTracksProvider.resolvePreviousTrack(targetAudioTrack)

    fun resolveNextTrack(targetAudioTrack: AudioTrack): AudioTrack =
            defaultAudioTracksProvider.resolveNextTrack(targetAudioTrack)
}