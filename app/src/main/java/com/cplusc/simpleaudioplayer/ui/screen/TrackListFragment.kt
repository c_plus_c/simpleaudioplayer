package com.cplusc.simpleaudioplayer.ui.screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cplusc.simpleaudioplayer.R
import com.cplusc.simpleaudioplayer.domain.AudioTrack
import com.cplusc.simpleaudioplayer.ui.activity.MainActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class TrackListFragment : Fragment() {

    companion object {
        fun newInstance() = TrackListFragment()
    }
    
    private val compositeDisposable = CompositeDisposable()

    private lateinit var viewModel: TrackListViewModel
    
    lateinit var clickedAudioTrackObservable: Observable<AudioTrack>


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.track_list_fragment, container, false)
        
        val recyclerView: RecyclerView = view.findViewById(R.id.track_list_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(context)
        
        val trackListAdapter = TrackListAdapter()
        recyclerView.adapter = trackListAdapter

        clickedAudioTrackObservable = trackListAdapter.clickedObservable

        activity?.let {
            viewModel = ViewModelProviders.of(it).get(TrackListViewModel::class.java)
        }

        if(activity is MainActivity){
            val mainActivity = activity as MainActivity
            mainActivity.playingAudioChangedObservable
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        trackListAdapter.setPlayingTrack(it)
                    }.addTo(compositeDisposable)
            
            mainActivity.playOrPauseChangedObservable
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        trackListAdapter.setPlayOrPause(it)
                    }.addTo(compositeDisposable)
        }

        val refreshLayout:SwipeRefreshLayout = view.findViewById(R.id.refresh_layout)

        refreshLayout.setOnRefreshListener {
            if(!viewModel.isLoading){
                view.findViewById<TextView>(R.id.error_notice).visibility = View.GONE
                viewModel.fetchAudioTracks()
            }else{
                refreshLayout.isRefreshing = false
            }
        }

        viewModel.audioTrackListObservable.observe(this, Observer {
            audioTrackList ->
            refreshLayout.isRefreshing = false
            audioTrackList?.let {
                trackListAdapter.setData(it)
            }
        })
        viewModel.errorObservable.observe(this, Observer {
            refreshLayout.isRefreshing = false
            view.findViewById<TextView>(R.id.error_notice).visibility = View.VISIBLE
            Toast.makeText(context, getString(R.string.error_notice), Toast.LENGTH_SHORT).show()
        })

        viewModel.fetchAudioTracks()
        
        return view
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
