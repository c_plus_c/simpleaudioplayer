package com.cplusc.simpleaudioplayer.domain


interface AudioTracksProvider {
    suspend fun fetchAudioTracks(): List<AudioTrack>
    fun resolvePreviousTrack(targetAudioTrack: AudioTrack): AudioTrack
    fun resolveNextTrack(targetAudioTrack: AudioTrack): AudioTrack
}