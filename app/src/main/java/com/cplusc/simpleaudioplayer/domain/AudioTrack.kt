package com.cplusc.simpleaudioplayer.domain

import android.net.Uri

data class AudioTrack(val title: String, var url: Uri)