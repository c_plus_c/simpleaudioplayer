package com.cplusc.simpleaudioplayer.infrastructure

import android.content.Context
import android.net.Uri
import com.cplusc.simpleaudioplayer.R
import com.cplusc.simpleaudioplayer.player.AudioPlayer
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class ExoPlayerAudioPlayer(val context: Context) : AudioPlayer {

    private val simpleExoPlayer: SimpleExoPlayer
    private val mediaSourceFactory: ExtractorMediaSource.Factory

    private var isPrepareFinished = false

    init {
        val bandwidthMeter = DefaultBandwidthMeter()
        val trackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(trackSelectionFactory)

        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector)

        val dataSourceFactory = DefaultDataSourceFactory(context, Util.getUserAgent(context, context.getString(R.string.app_name)), bandwidthMeter)
        mediaSourceFactory = ExtractorMediaSource.Factory(dataSourceFactory)


        simpleExoPlayer.addListener(object: Player.EventListener{
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
                //do nothing
            }

            override fun onSeekProcessed() {
                //do nothing
            }

            override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
                //do nothing
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                //do nothing
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                //do nothing
            }

            override fun onPositionDiscontinuity(reason: Int) {
                //do nothing
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
                //do nothing
            }

            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
                //do nothing
            }

            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
                //do nothing
            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == Player.STATE_ENDED) {
                    simpleExoPlayer.playWhenReady = true
                    simpleExoPlayer.seekTo(0)
                }
            }
        })
    }

    override suspend fun load(uri: Uri) = suspendCoroutine<Unit> { continuation ->
        val mediaSource = mediaSourceFactory.createMediaSource(uri)
        isPrepareFinished = false
        simpleExoPlayer.prepare(mediaSource)
        simpleExoPlayer.playWhenReady = true

        val listener = object : Player.EventListener {
            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {
                //do nothing
            }

            override fun onSeekProcessed() {
                //do nothing
            }

            override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
                //do nothing
            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                //do nothing
            }

            override fun onLoadingChanged(isLoading: Boolean) {
                //do nothing
            }

            override fun onPositionDiscontinuity(reason: Int) {
                //do nothing
            }

            override fun onRepeatModeChanged(repeatMode: Int) {
                //do nothing
            }

            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {
                //do nothing
            }

            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {
                //do nothing
            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == Player.STATE_READY && !isPrepareFinished) {
                    isPrepareFinished = true
                    simpleExoPlayer.removeListener(this)
                    continuation.resume(Unit)
                }
            }
        }
        simpleExoPlayer.addListener(listener)
    }

    override fun play() {
        simpleExoPlayer.playWhenReady = true
    }

    override fun pause() {
        simpleExoPlayer.playWhenReady = false
    }

    override fun seekTo(positionInMillisecond: Long) =
            simpleExoPlayer.seekTo(positionInMillisecond)


    override fun getCurrentPositionInMillisecond(): Long =
            simpleExoPlayer.currentPosition


    override fun release() =
            simpleExoPlayer.release()

    override fun getDuration(): Long =
            simpleExoPlayer.duration

}