package com.cplusc.simpleaudioplayer.infrastructure

import android.net.Uri
import com.cplusc.simpleaudioplayer.domain.AudioTrack
import com.cplusc.simpleaudioplayer.domain.AudioTracksProvider
import com.github.kittinunf.fuel.android.extension.responseJson
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


internal class AudioTracksProviderFuelImpl : AudioTracksProvider {

    override suspend fun fetchAudioTracks() = suspendCoroutine<List<AudioTrack>> { continuation ->

        "http://logbox.sakura.ne.jp/misc/track_list.json".httpGet().responseJson { _, _, result ->

            when(result){
                is Result.Failure -> {
                    continuation.resumeWithException(result.getException())
                }
                is Result.Success -> {

                    val rootArray = result.component1()?.array()
                    rootArray?.let {
                        for (i in 0..(rootArray.length() - 1)) {
                            val element = rootArray.getJSONObject(i)
                            element?.let {
                                val name = element.getString("name")
                                val uri = element.getString("uri")

                                mutableAudioTracksList.add(AudioTrack(name, Uri.parse(uri)))
                            }
                        }
                        continuation.resume(mutableAudioTracksList)
                    }
                }
            }
        }
    }


    
    override fun resolvePreviousTrack(targetAudioTrack: AudioTrack): AudioTrack {
        val foundIndex = mutableAudioTracksList.indexOf(targetAudioTrack)
        if(foundIndex == 0){
            return mutableAudioTracksList.last()
        }else{
            return mutableAudioTracksList[(foundIndex - 1) % mutableAudioTracksList.size]
        }
    }
    
    override fun resolveNextTrack(targetAudioTrack: AudioTrack): AudioTrack {
        val foundIndex = mutableAudioTracksList.indexOf(targetAudioTrack)
        return mutableAudioTracksList[(foundIndex + 1) % mutableAudioTracksList.size]
    }

    private var mutableAudioTracksList = mutableListOf<AudioTrack>()
}