package com.cplusc.simpleaudioplayer.player

import android.net.Uri

interface AudioPlayer{

    suspend fun load(uri: Uri)

    fun play()

    fun pause()

    fun seekTo(positionInMillisecond: Long)

    fun getCurrentPositionInMillisecond(): Long

    fun getDuration(): Long

    fun release()


}