package com.cplusc.simpleaudioplayer.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import com.cplusc.simpleaudioplayer.domain.AudioTrack
import com.cplusc.simpleaudioplayer.infrastructure.ExoPlayerAudioPlayer
import com.cplusc.simpleaudioplayer.player.AudioPlayer
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


class AudioPlayService : Service() {

    inner class LocalBinder : Binder() {
        fun getService(): AudioPlayService =
                this@AudioPlayService
    }

    private lateinit var audioPlayer: AudioPlayer

    lateinit var currentPlayingAudioTrack: AudioTrack

    private val loadFinishedSubject: Subject<Unit> = PublishSubject.create()
    val loadFinishedObservable: Observable<Unit>
        get() = loadFinishedSubject


    private val currentPositionChangedSubject: Subject<Long> = PublishSubject.create()
    val currentPositionChangedObservable: Observable<Long>
        get() = currentPositionChangedSubject

    private val currentPositionUpdateTimer = Timer()

    override fun onCreate() {
        super.onCreate()

        audioPlayer = ExoPlayerAudioPlayer(applicationContext)

        val notificationHelper = NotificationHelper(this)
        val builder = notificationHelper.createNotificationBuilder()
        startForeground(2, builder.build())
    }

    private val binder = LocalBinder()

    override fun onBind(intent: Intent): IBinder =
            binder

    fun load(audioTrack: AudioTrack) {
        currentPlayingAudioTrack = audioTrack
        GlobalScope.launch {
            audioPlayer.load(currentPlayingAudioTrack.url)
            currentPositionUpdateTimer.scheduleAtFixedRate(object : TimerTask() {
                override fun run() {
                    currentPositionChangedSubject.onNext(audioPlayer.getCurrentPositionInMillisecond())
                }
            }, 10, 10)
            loadFinishedSubject.onNext(Unit)
        }
    }

    fun play() {
        audioPlayer.play()
    }

    fun pause() {
        audioPlayer.pause()
    }

    fun seekTo(positionInMillisecond: Long) {
        audioPlayer.seekTo(positionInMillisecond)
    }

    override fun onDestroy() {
        super.onDestroy()
        audioPlayer.release()
    }

    fun getDurationInMilliSecond() =
            audioPlayer.getDuration()
}
