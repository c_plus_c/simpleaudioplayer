package com.cplusc.simpleaudioplayer.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import androidx.core.app.NotificationCompat
import com.cplusc.simpleaudioplayer.R

internal class NotificationHelper(private val context: Context): ContextWrapper(context){

    private var notificationManager: NotificationManager? = null

    private fun getManager(): NotificationManager?{
        if(notificationManager == null){
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        return notificationManager
    }

    init {
        if(isOreoOrLater()){
            val notificationChannel = NotificationChannel(CHANNEL_GENERAL_ID, "General Notifications", NotificationManager.IMPORTANCE_DEFAULT)
            getManager()?.createNotificationChannel(notificationChannel)
        }
    }

    fun createNotificationBuilder(): NotificationCompat.Builder {
        val builder = NotificationCompat.Builder(this, CHANNEL_GENERAL_ID)

        return builder.setContentTitle(getString(R.string.app_name))
                .setContentText("Hello World!")
                .setSmallIcon(R.mipmap.ic_launcher)
    }

    private fun isOreoOrLater(): Boolean =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.O

    companion object {
        private val CHANNEL_GENERAL_ID = "general"
    }
}